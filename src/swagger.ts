import swaggerJSDoc from 'swagger-jsdoc';

const swaggerDefinition = {
    info: {
        title: 'API toro service',
        version: '1.0.0',
    },
    host: 'localhost:3333',
    basePath: '/',
};

const options = {
    swaggerDefinition,
    apis: ['./src/routes/user.ts'],
};

export const swaggerSpec = swaggerJSDoc(options);
