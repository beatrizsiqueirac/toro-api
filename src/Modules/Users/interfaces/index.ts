export interface UserInterface {
    id?: string;
    name: string;
    email: string;
    password?: string;
    cpf: string;
    balance: number;
}

export type QueryUserInterface = Partial<Omit<UserInterface, 'password'>>;

export type CreateUserInterface = Omit<UserInterface, 'id'>;

export type UpdateUserInterface = Partial<CreateUserInterface>;

export type UserReturnColumns = 'name' | 'email' | 'id' | 'cpf' | 'balance';
