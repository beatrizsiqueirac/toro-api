import { UserInterface } from '../interfaces';

export const removeSecretProperties = (user: UserInterface): UserInterface => {
    const userCpy = { ...user };

    delete userCpy.id;
    delete userCpy.password;

    return userCpy;
};
