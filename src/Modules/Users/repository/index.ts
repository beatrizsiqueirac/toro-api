import { createQueryBuilder, DeleteResult, getRepository } from 'typeorm';
import { User } from '../../../database/entities/User';
import {
    CreateUserInterface,
    UserInterface,
    QueryUserInterface,
    UpdateUserInterface,
    UserReturnColumns,
} from '../interfaces';

export const create = async (
    userObject: CreateUserInterface,
    returning: '*' | UserReturnColumns[] = '*',
): Promise<UserInterface> => {
    const userEntity = Object.assign(new User(), userObject);

    const { raw } = await createQueryBuilder(User)
        .insert()
        .values(userEntity)
        .returning(returning)
        .execute();

    return raw[0];
};

export const getOne = async (
    query: QueryUserInterface,
    returning?: UserReturnColumns[],
): Promise<User | undefined> => {
    return getRepository(User).findOne(query, {
        select: returning,
    });
};

export const update = async (
    id: string,
    updates: UpdateUserInterface,
    returning: '*' | UserReturnColumns[] = ['name', 'email', 'id', 'cpf', 'balance'],
): Promise<User | undefined> => {
    const updateRes = await createQueryBuilder()
        .update(User, Object.assign(new User(), updates))
        .where({ id })
        .returning(returning)
        .execute();

    return updateRes.raw;
};

export const deleteUser = async (id: string): Promise<DeleteResult> => {
    return getRepository(User).delete({ id });
};
