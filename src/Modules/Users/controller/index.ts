import { User } from '../../../database/entities/User';
import { HttpError } from '../../../utils/errors/HttpError';
import {
    UserInterface,
    QueryUserInterface,
    UpdateUserInterface,
} from '../interfaces';
import * as handler from '../handler/index';

import * as repository from '../repository';
import { authenticate } from '../utils/auth';

export const auth = async (
    email: string,
    password: string,
): Promise<{ user?: User; token?: string }> => {
    const user = await repository.getOne({ email });

    return authenticate(user!, password);
};

export const getUserPosition = async (cpf: string): Promise<any> => {
    const amount = await handler.getUserPosition(cpf);
    return amount;
};

export const create = async (userBody: UserInterface): Promise<UserInterface> => {
    try {
        return await repository.create(userBody, [
            'email',
            'name',
            'id',
            'cpf',
            'balance',
        ]);
    } catch {
        throw new HttpError(400, 'Já existe usuário com este email');
    }
};

export const getOne = async (
    query: QueryUserInterface,
): Promise<UserInterface | undefined> => {
    const res = await repository.getOne(query, ['cpf', 'name', 'email']);

    if (!res) throw new HttpError(404, 'User Not Found!');

    return res;
};

export const update = async (
    id: string,
    updates: UpdateUserInterface,
): Promise<UserInterface | undefined> => {
    const res = await repository.update(id, updates, [
        'email',
        'name',
        'id',
        'cpf',
        'balance',
    ]);

    if (!res) throw new HttpError(404, 'User Not Found!');

    return res;
};

export const deleteUser = async (id: string): Promise<void> => {
    const deleteResult = await repository.deleteUser(id);

    if (deleteResult.affected !== 1) throw new HttpError(404, 'Not found');
};
