
import * as repository from '../repository';
import allActives from '../../../utils/Actives.json';

export const getUserPosition = async (cpf: string): Promise<any> => {
    const user = await repository.getOne({ cpf });
    let invested = 0;
    let lastCurrentPrice = 0;
    const userActives = JSON.parse(user?.actives || "");
    const positions: any[] = [];
    const balance = userActives.forEach((active: any) => {
        let amount = 0;
        const activesNumber = userActives.forEach((name: any) => {
            if (name === active) {
                amount += 1;
            }
        });
        allActives.forEach((element: { symbol: any; currentPrice: number }) => {
            if (element.symbol === active) {
                invested += element.currentPrice;
                lastCurrentPrice = element.currentPrice;
            }
        });
        const position = {
            symbol: active,
            amount,
            currentPrice: lastCurrentPrice,
        };
        positions.push(position);
    });
    return {
        consolidated: parseFloat(user?.balance) + invested,
        positions,
        checkingAccountAmount: parseFloat(user?.balance)
    };
};
