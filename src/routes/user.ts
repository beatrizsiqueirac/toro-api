/* eslint-disable max-len */
import { Request, Response, Router } from 'express';
import * as controller from '../Modules/Users/controller';
import * as validators from '../Modules/Users/validator';
import { getValidData } from '../utils/validator';
import { authMidleware } from '../Modules/Users/utils/auth';

const router = Router();

/**
 * @swagger
 *  definitions:
 *   User:
 *    properties:
 *     id:
 *      type: string
 *      description: Identificador do usuario
 *      example: 05fb8d22-e1b3-4691-8795-de87f65c0849
 *     name:
 *      type: string
 *      description: Nome do usuario
 *      example: Joao
 *     email:
 *      type: string
 *      description: Email do usuario
 *      example: joao@provider.com
 *     cpf:
 *      type: string
 *      description: CPF do usuario
 *      example: 11100033333
 *     password:
 *      type: string
 *      description: Senha do usuario
 *      example: senha
 *   CreateUser:
 *    properties:
 *     name:
 *      type: string
 *      description: Nome do usuario
 *      required: true
 *      example: Joao
 *     email:
 *      type: string
 *      description: Email do usuario
 *      required: true
 *      example: joao@provider.com
 *     cpf:
 *      type: string
 *      description: CPF do usuario
 *      required: true
 *      example: 11100033333
 *     password:
 *      type: string
 *      description: Senha do usuario
 *      required: true
 *      example: senha
 *   BadRequest:
 *    properties:
 *     code:
 *      type: number
 *      description: Codigo da resposta http
 *      required: true
 *      example: 400
 *     message:
 *      type: string
 *      description: Descrição do erro
 *      required: true
 *      example: Bad Request
 *     errors:
 *       type: array
 *       items:
 *          properties:
 *            msg:
 *              type: string
 *              example: Invalid value
 *            param:
 *              type: string
 *              example: password
 *            location:
 *              type: string
 *              example: body
 */

router.post(
    '/authenticate',
    validators.authValidator,
    async (req: Request, res: Response) => {
        const { body } = getValidData(req);

        const { token, user } = await controller.auth(body.email, body.password);
        const responseUser = {
            email: user?.email,
            name: user?.name,
            cpf: user?.cpf,
            balance: user?.balance,
        };
        return res.status(200).send({ token, ...responseUser });
    },
);

router.get(
    '/userPosition',
    validators.positionValidator,
    async (req: Request, res: Response) => {
        const { query } = getValidData(req);
        const response = await controller.getUserPosition(query.cpf);
        return res.status(200).send(response);
    },
);

/**
 * @swagger
 * /api/users:
 *   post:
 *     tags:
 *       - User
 *     description: Criação de usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: JSON with user attributes.
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/CreateUser'
 *     responses:
 *       201:
 *         description: Successfull
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Bad Request
 *         schema:
 *           $ref: '#/definitions/BadRequest'
 */
router.post('/', validators.createValidator, async (req: Request, res: Response) => {
    const { body } = getValidData(req);

    const createdUser = await controller.create(body);

    return res.status(201).send(createdUser);
});

router.use(authMidleware);

router.get('/', validators.queryValidator, async (req: Request, res: Response) => {
    const { query } = getValidData(req);

    const findedUser = await controller.getOne(query);

    return res.status(200).send(findedUser);
});

router.put(
    '/:id',
    validators.updateValidator,
    async (req: Request, res: Response) => {
        const { params, body } = getValidData(req);

        const updatedUser = await controller.update(params.id, body);

        return res.status(200).send(updatedUser);
    },
);

router.delete(
    '/:id',
    validators.deleteValidator,
    async (req: Request, res: Response) => {
        const { params } = getValidData(req);

        await controller.deleteUser(params.id);

        return res.status(200).send();
    },
);

export default router;
