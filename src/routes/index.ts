import { Request, Response, Router } from 'express';
import swaggerUi from 'swagger-ui-express';
import { swaggerSpec } from '../swagger';
import userRoutes from './user';

const router = Router();

router.get('/', (req: Request, res: Response) => {
    res.send('Toro Service 0.01');
});

router.use('/api/users', userRoutes);

router.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

export default router;
