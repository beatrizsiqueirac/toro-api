import { UserInterface } from '../../Modules/Users/interfaces';
import { User } from '../entities/User';

export default Object.assign(new User(), {
    id: 'b485b977-92e8-4dc3-86dc-65c2333d7d0e',
    email: 'bia@toroinvestimentos.com.br',
    name: 'Beatriz',
    password: 'beatriz123',
    cpf: '13370650213',
    balance: 1000.0,
    actives: '["PETR4", "MGLU3", "VVAR3", "SANB11"]',
} as UserInterface);
