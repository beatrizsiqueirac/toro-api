import { createQueryBuilder, getRepository, MigrationInterface, QueryBuilder, QueryRunner } from "typeorm";
import { User } from "../entities/User";
import masterUser from '../seeds/users.seed'

export class seedMasterUser9999999999999 implements MigrationInterface {
    private createdUser: User | undefined;

    public async up(queryRunner: QueryRunner): Promise<void> {
        await masterUser['hashPassword']()

        const [sql, paramters] = createQueryBuilder(User).insert().values(masterUser).getQueryAndParameters()

        this.createdUser = await queryRunner.query(sql, paramters)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await getRepository(User).delete({ id: this.createdUser?.id })
    }

}
