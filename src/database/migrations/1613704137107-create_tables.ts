import {MigrationInterface, QueryRunner} from "typeorm";

export class createTables1613704137107 implements MigrationInterface {
    name = 'createTables1613704137107'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "persons" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "cpf" character varying NOT NULL, "balance" character varying NOT NULL, "actives" character varying NOT NULL, CONSTRAINT "UQ_928155276ca8852f3c440cc2b2c" UNIQUE ("email"), CONSTRAINT "UQ_928155276ca8852f3c440cc8b2c" UNIQUE ("cpf"), CONSTRAINT "PK_74278d8812a049233ce41440ac7" PRIMARY KEY ("id"))`);
        }

    public async down(queryRunner: QueryRunner): Promise<void> {
       await queryRunner.query(`DROP TABLE "persons"`);
    }

}
